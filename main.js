//getting the required html elements
const checkbox = document.getElementById("check");
const basicPrice = document.querySelector(".basicPrice");
const professionalPrice = document.querySelector(".professionalPrice");
const masterPrice = document.querySelector(".masterPrice");

let count = 0;

//adding event listener to the checkbox
checkbox.addEventListener("change", function (e) {
  if (count % 2 == 0) {
    basicPrice.innerHTML = "<span>&dollar;</span>19.99";
    professionalPrice.innerHTML = "<span>&dollar;</span>24.99";
    masterPrice.innerHTML = "<span>&dollar;</span>39.99";
  } else {
    basicPrice.innerHTML = "<span>&dollar;</span>199.99";
    professionalPrice.innerHTML = "<span>&dollar;</span>249.99";
    masterPrice.innerHTML = "<span>&dollar;</span>399.99";
  }
  count++;
});
